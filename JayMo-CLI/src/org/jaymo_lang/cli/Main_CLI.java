/*******************************************************************************
 * Copyright (C): 2017-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-CLI <https://www.jmo-lang.org>.
 *
 * JayMo-CLI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-CLI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-CLI. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.cli;

import java.io.IOException;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.error.ErrorBaseDebug;
import org.jaymo_lang.model.App;
import org.jaymo_lang.parser.Parser_App;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MOut;
import de.mn77.base.version.Lib_Version;
import de.mn77.base.version.data.VersionData_ABC;
import de.mn77.lib.terminal.CSI_COLOR_BG;
import de.mn77.lib.terminal.CSI_COLOR_FG;
import de.mn77.lib.terminal.MTerminal;


/**
 * @author Michael Nitsche
 * @created 11.10.2017
 */
public class Main_CLI {

	private static final VersionData_ABC VERSION       = new VersionData_ABC( 0, 11, 0 );
	private static final String          PROMPT        = "JM › "; // "JM > " "JayMo> " "JM> "
	private static final String          RESULT_PREFIX = "   = "; //"=>  "
	private static final Object          EXIT_MESSAGE  = "Have a nice day!"; //"Good Bye!";


	public static void main( final String[] args ) {

		// String[]{"-d","-p1","4","-p2","3","-m","2"};//,"-a","/mnt/musik"};
		try {
			Lib_Version.init( Main_CLI.VERSION, false );

			final Main_CLI cli = new Main_CLI();
			cli.start( false, false, false, false );
		}
		catch( final Throwable e ) {
			Err.exit( e );
		}
	}

	public boolean runTerminal( final Parser_App parser, final MTerminal terminal, final String in, final boolean parseOnly ) throws IOException, Err_FileSys {
		final String userInput = in != null
			? in
			: this.iReadLine( terminal );

		// If it's still null after read:
		if( userInput == null || userInput.equals( "exit" ) || userInput.equals( "quit" ) || userInput.equals( "q" ) ) { // || uin.equals("_APP.exit") || uin.equals("§exit")  // uin.equals("e")
// 			terminal.print("\n", CSI.cursorToRow(1));
			terminal.print( "\r\n" );
			terminal.closeTerminal();
			return false;
		}

		final boolean rawBefore = terminal.temporaryCooked();
		final Group2<String, Boolean> result = this.iExec( parser, userInput, parseOnly );
		terminal.temporaryReset( rawBefore );

		final boolean execOkay = result.o2;
		String output = parseOnly
			? execOkay
				? "Parsing okay"
				: "Parsing failed"
			: result.o1;

		// Terminal can be null, if argument is used
		// Color is only possible in raw mode!
		if( terminal != null )
			if( terminal.isInRawMode() ) {
				final CSI_COLOR_FG color = execOkay ? CSI_COLOR_FG.GREEN : CSI_COLOR_FG.RED;
				output = Lib_String.replace( output, '\n', "\n\r" );
				terminal.print( color, "\r" + Main_CLI.RESULT_PREFIX, output, "\r\n", CSI_COLOR_FG.DEFAULT );
			}
			else
				terminal.print( Main_CLI.RESULT_PREFIX, output, "\n" );

		return true;
	}

	public void start( final boolean parseOnly, final boolean debug, final boolean noHeader, final boolean afterExec ) throws IOException, Err_FileSys {
		MOut.reset();
		MOut.setJavaErrors( false );
		Lib_Version.initKeep( Main_CLI.VERSION );

		final Parser_App parser = new Parser_App( false ); // false = No build date and info is available --> but it's faster

		// start CLI
		final MTerminal terminal = new MTerminal();
		if( debug )
			terminal.setDebug();
		parser.setTerminalRawMode( terminal.isInRawMode() );

		Runtime.getRuntime().addShutdownHook( new Thread() {

			@Override
			public void run() {
				terminal.closeTerminal();
			}

		} );

		if(afterExec)
			terminal.print( "\r\n" );
		if(!noHeader)
			this.iPrintHeadLine( terminal, parser );

		boolean run = true;
		while( run )
			run = this.runTerminal( parser, terminal, null, parseOnly );

		MOut.print( Main_CLI.EXIT_MESSAGE );
	}

	private Group2<String, Boolean> iExec( final Parser_App parser, final String s, final boolean parseOnly ) throws Err_FileSys {
		String result = null;
		boolean ok = false;

		try {
			final App app = parser.parseText( s );
			result = parseOnly
				? null
				: app.exec( null );
			ok = true;
		}
		catch( final ErrorBaseDebug t ) {
			MOut.print( t.toInfo() );
//			result = t.getClass().getSimpleName(); // "Java-Error";
			result = "Error";
		}
		catch( final Throwable t ) {
			final String errMsg = "Error   : Java-Error\r\n"; //+MOut.getLineBreak()
			MOut.print( errMsg + t.getMessage() );
//			Err.show(t);
//			result = t.getClass().getSimpleName(); // "Java-Error";
			result = "Error";
		}

		if( result == null )
			result = "";

		// Limit result to one line!
		final int nextLineBreak = result.indexOf( '\n' );
		if( nextLineBreak >= 0 )
			result = result.substring( 0, nextLineBreak ) + "¶…"; // ¶ = Linebreak, … = something else

		return new Group2<>( result, ok );
	}

	private void iPrintHeadLine( final MTerminal terminal, final Parser_App parser ) {
		final String version_parser = parser.getVersionString( JayMo.STAGE.isDevelopment(), false, false);
		final String version_cli = "CLI " + Main_CLI.VERSION.toStringShort();//.toFormat("CLI %1.%2.%3"); // (%b)
//		final String version_date = parser.getVersion().toFormat("%dd.mm.yyyy");

		final String delimiter = "  /  ";

		if( terminal.isInRawMode() ) {
			terminal.print( CSI_COLOR_BG.BLUE );
//			terminal.print(" ");
			terminal.print( CSI_COLOR_FG.WHITE, JayMo.NAME );
//			terminal.print(" ");

			if( JayMo.STAGE.isDevelopment() ) {
				terminal.print( CSI_COLOR_BG.DEFAULT );
				terminal.print( "-" );
				terminal.print( JayMo.STAGE.isCritical() ? CSI_COLOR_BG.RED : CSI_COLOR_BG.YELLOW );
				terminal.print( CSI_COLOR_FG.BLACK, JayMo.STAGE.text.toUpperCase() );
			}

			terminal.print( CSI_COLOR_BG.DEFAULT );
			terminal.print( "  " );
			terminal.print( parser.isDevVersion() ? CSI_COLOR_FG.YELLOW : CSI_COLOR_FG.GREEN ); // BROWN	YELLOW LIGHTGREEN
			terminal.print( version_parser );
			terminal.print( CSI_COLOR_FG.DARKGRAY, delimiter );
			terminal.print( Main_CLI.VERSION.isDevelopment() ? CSI_COLOR_FG.YELLOW : CSI_COLOR_FG.GREEN ); //LIGHTGREEN
			terminal.print( version_cli ); // CYAN
			terminal.print( CSI_COLOR_FG.DARKGRAY, delimiter );
			terminal.print( CSI_COLOR_FG.BLUE ); //LIGHTBLUE
			terminal.print( JayMo.WEB_HOME_SHORT );

			terminal.print( "\r\n\n" );
		}
		else {
			final StringBuilder sb = new StringBuilder();
			sb.append( JayMo.NAME );

			if( JayMo.STAGE.isDevelopment() )
				sb.append("-"+JayMo.STAGE.text.toUpperCase() );

			sb.append( "  " );
			sb.append( version_parser );
			sb.append( delimiter );
			sb.append( version_cli );
			sb.append( delimiter );
			sb.append( JayMo.WEB_HOME_SHORT );
			sb.append( "\n\n" );
			terminal.print( sb.toString() );
		}
	}

	/**
	 * @return Returns the user input. Result can be null
	 */
	private String iReadLine( final MTerminal terminal ) throws IOException {

		if( terminal.isInRawMode() ) {
			terminal.print( CSI_COLOR_BG.DEFAULT, CSI_COLOR_FG.LIGHTBLUE, Main_CLI.PROMPT, CSI_COLOR_FG.DEFAULT );
			final String input = terminal.readLine();
			terminal.print( "\r\n" );
			return input;
		}
		else {
			terminal.print( Main_CLI.PROMPT );
			return terminal.readLine();
		}
	}

}
