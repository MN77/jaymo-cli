/*******************************************************************************
 * Copyright (C): 2017-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of JayMo-CLI <https://www.jmo-lang.org>.
 *
 * JayMo-CLI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo-CLI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JayMo-CLI. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.cli;

import java.io.IOException;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.cli.info.Lib_CliInfo;
import org.jaymo_lang.model.App;
import org.jaymo_lang.parser.Parser_App;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.base.data.convert.ConvertArray;
import de.mn77.base.data.util.U_StringArray;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.sys.ArgumentParser;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.I_File;
import de.mn77.base.sys.file.MFile;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class Main_JayMo {

	private boolean parseOnly        = false;
	private String  directExec       = null;
	private boolean directExecResult = false;
	private boolean debug            = false;
	private boolean runCLI           = false;
	private boolean noHeader         = false;


	public static void main( final String[] args ) {
		MOut.reset();
		MOut.setJavaErrors( false );
//		MOut.setLineBreak("\r\n");

		try {
			final Main_JayMo jmo = new Main_JayMo();
			jmo.start( args );
		}
		catch( final Throwable t ) {
			Err.exit( Lib_Error.wrap( t ) );
		}
	}

	public void start( String[] args ) throws Err_FileSys, IOException {
		args = this.iParseArgs( args );

		if( args == null ) // No execution, direct exit
			return;

		final boolean exec = args.length > 0 || this.directExec != null;

		if(exec)
			this.iDirectExec( args );

		if( this.runCLI || !exec ) {
			final Main_CLI cli = new Main_CLI();
			cli.start( this.parseOnly, this.debug, this.noHeader, exec );
		}
	}

	private void iDirectExec( String[] args ) throws Err_FileSys {
		// Execute given string or file
		final Parser_App parser = new Parser_App( false ); // Don't read build informations --> really faster
		if( this.debug )
			parser.setDebug();

		App app = null;

		if( this.directExec != null )
			app = parser.parseText( this.directExec.trim() );
		else {
			final I_File file = new MFile( args[0] ); // [0] = File, [1-] = Arguments

			if( !file.exists() ) {
				MOut.print( "Missing file: " + file.getPathAbsolute() );
				return;
			}

			app = parser.parseFile( file.getFile() );

			// Remove File from Args
			args = U_StringArray.cutFrom( args, 1 );
		}

		// Execute
		if( this.parseOnly )
			MOut.print( "Test okay, no parse errors found." );
		else {
			final String result = app.exec( args );
			if( this.directExecResult && result != null )
				MOut.print( "Result: " + result );
		}
	}

	/**
	 * @return Returns an String[] with all given File-Arguments. If the app should exit directly, null will be returned.
	 * @implNote
	 *           commons-cli? Would be nice, but currently jmo-core should be only one single jar, without dependencies.
	 */
	private String[] iParseArgs( final String[] args ) {
		final ArgumentParser arguments = new ArgumentParser(true, false, true);
		arguments.parse(args);

		for(String s : arguments.getSwitches()) {
			switch( s ) {
				case "t":
				case "test":
					this.parseOnly = true;
					break;
				case "d":
				case "debug":
					this.debug = true;
					MOut.setJavaErrors( true );
					MOut.setDebug();
					break;
				case "c":
				case "cli":
					this.runCLI = true;
					break;
				case "n":
				case "noheader":
					this.noHeader = true;
					break;
				case "E":
				case "execout":
					this.directExecResult = true;
					this.iInitExec(arguments);
					break;
				case "e":
				case "exec":
					this.iInitExec(arguments);
					break;

				// -- Print and Exit --
				case "license":
					final String licenseMessage = JayMo.NAME + "  Copyright (C):  " + JayMo.COPYRIGHT_YEAR + "  " + JayMo.COPYRIGHT_NAME + "\n\n" + Lib_CliInfo.license();
					MOut.print( licenseMessage );
					return null;
				case "version":
//					MOut.print( JayMo.NAME + "  " + new Parser_App().getVersionString( JayMo.STAGE.isDevelopment(), false, true ) );
					MOut.print( JayMo.NAME + "  " + new Parser_App().getVersionString( true, true, true ) );
					return null;
				case "help":
					MOut.print( Lib_CliInfo.help() );
					return null;

				default:
					throw new Err_Runtime( "Unknown argument for " + JayMo.NAME, s );
			}
		}

		// Script-files to start
		final String[] result = arguments.getOthers();
		return result != null
			? result
			: new String[0];
	}

	private void iInitExec(ArgumentParser arguments) {
		String[] other = arguments.getOthers();
		if( other == null || other.length == 0 )
			throw new Err_Runtime( "Invalid arguments", "Missing code " );

		this.directExec = ConvertArray.toString( " ", other );
	}

}
